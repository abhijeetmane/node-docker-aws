1. command to start Mongodb daemon
mongod --dbpath /Users/nandha/myProjects/mongodbData;
--waiting for connections on port means server started
2.Provide mongodb path
mongod --dbpath c:\node\nodetest2\data

3. Create Docker file
   docker build -t abhijeet/nodetest2 .

4. docker images
5. select image you want to run and type
    1. Detach mode : docker run -p 3000:3000 -d abhijeet/nodetest2
    2. Interactive mode :  docker run -p 3000:3000 -it abhijeet/nodetest2
6. docker container ps
7. docker logs <container id>
8. explore work directory of docker container
    1. docker exec -it 3fb77bb62197 /bin/bash
9.  Stop container
    1.  docker container stop containerID
    2.  delete containers 
    docker rm $(docker container ps -a)
    3. docker rmi ${image_id}

    
