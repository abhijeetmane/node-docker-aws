# Node v7 as the base image to support ES6
FROM node:8

ENV HOME=/Users/nandha/myProjects/nodejs/nodetest2

WORKDIR $HOME/src

COPY . .

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]